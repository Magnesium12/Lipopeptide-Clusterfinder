# Lipopeptide-Clusterfinder

## Abstract

Some Bacteria are producing a bunch of interesting lipopeptides with a large variant of properties.
It is sometimes difficult to recognize this substances in mass spectra.
The goal of this Project is to build an algorithm for recognition of this substance class by using the typical mass distribution of synthesized species and ionisation characteristics. To demonstrate functioning of this algorithm data from @Kott2018 is used.
In this documentation we will only deal with the functions and operation of the developed algorithm. For more information on the investigated lipopeptides and their properties, see [@Stein2005, @Caulier2019, @Cochrane2016].

For more details, see README.html
