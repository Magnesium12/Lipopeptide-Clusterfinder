#!/bin/bash
pdftk Diagrams.pdf burst
rm doc_data.txt
for f in pg_*.pdf
do
    echo "$f"' to jpg'
    convert -density 600 "$f" "$(basename -s .pdf "$f")".jpg
done
echo 'to svg'
pdf2svg ./Diagrams.pdf ./Diagrams.svg

exit 0
