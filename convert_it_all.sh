#!/bin/bash
parseDir() {
	FORCE="FALSE"
	tmp="$(mktemp -d --suffix=_-_"$(basename "$0")")"
	
	for obj in "$@"
    do
		if [ -d "$obj" ]
        then
			dir_name="$obj"
			echo "Betrete \"""$dir_name""\""
			cd "$dir_name"
			parseDir *
			senti=0
			for f in *
			do
				if ! ([ -f "$f" ]) # && ([ "$(echo "$f" | grep -cP '.jpg\$')" -ge "1" ] || [ "$(echo "$f" | grep -cP '.png\$')" -ge "1" ] || [ "$(echo "$f" | grep -cP '.jpeg\$')" -ge "1" ] || [ "$(echo "$f" | grep -cP '.pdf\$')" -ge "1" ]))
				then 
					senti=1
					break
				fi
			done 
			if [ "$senti" == 0 ]
            then
				if [ ! -f ../"$dir_name".pdf ] || [ "$FORCE" == "TRUE" ]
				then
					echo "Erzeuge ""\""../"$dir_name".pdf"\""
					pdftk *.pdf cat output ../"$dir_name".pdf
					rm *.pdf
				fi
				if [ ! -f ../"$dir_name".cbz ] || [ "$FORCE" == "TRUE" ]
				then
					echo "Erzeuge ""\""../"$dir_name".cbz"\""
					zip ../"$dir_name".cbz *.jpg *.png *.jpeg *.webp *.tif *.tiff
				fi
			fi
		 	cd ..
		elif [ -f "$obj" ]
        then
			#ext=${obj/*./}
            #path_of_obj=$(readlink -f "$obj")
			case "$obj" in
			*.jpg|*.jpeg|*.webp)				
				if ( [ ! -f "$obj".pdf ] && ( [ ! -f ../"$(basename "$(dirname "$(readlink -f "$obj")")".pdf)" ] || [ ! -f ../"$(basename "$(dirname "$(readlink -f "$obj")")".cbz)" ] ) ) || [ "$FORCE" == "TRUE" ]
				then
					echo "Konvertiere ""$obj"" zu PDF"
					convert -quality 40 "$obj" "$tmp"/"$obj"_tmpcia.jpg
					convert "$tmp"/"$obj"_tmpcia.jpg "$obj".pdf # 40 für Manga Rock
					rm "$tmp"/"$obj"_tmpcia.jpg
				fi
			;;
			*.png|*.tif|*.tiff)
			if ( [ ! -f "$obj".pdf ] && ( [ ! -f ../"$(basename "$(dirname "$(readlink -f "$obj")")".pdf)" ] || [ ! -f ../"$(basename "$(dirname "$(readlink -f "$obj")")".cbz)" ] ) ) || [ "$FORCE" == "TRUE" ]
				then
					echo "Konvertiere ""$obj"" zu PDF"
					convert "$obj" "$obj".pdf					
				fi
            ;;
			*)			
				:
				#echo "Unbekannte Datei"
			;;
			esac	
		fi
	done
	rm -rv "$tmp"
}

parseDir "$@"
