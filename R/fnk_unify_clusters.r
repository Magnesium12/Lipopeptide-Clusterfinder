#' Create Color patterns for report
#' @param report A return of find_cluster()
#' @param method Color Methods
#' @param labels Annotate Peaks
#'
#' @return A Number defining matching clusters
#' @export
unify_cluster <- function(report, method = "trunc", labels = TRUE) {
  switch (method,
    "CH2" = {
      if (isTRUE(labels)) vNames <- paste("+", 14*report$dbase_CH2, "Da", sep = "")
      return(list(
        affnum = report$cluster_CH2,
        affchar = paste("Cluster", report$cluster_CH2),
        name = vNames
      ));
    },
    "C" = {
      if (isTRUE(labels)) vNames <- paste("+", report$dbase_C, "Da", sep = "");
      return(list(
        affnum = report$cluster_C,
        affchar = paste("Cluster", report$cluster_C),
        name = vNames
      ));
    },
    "C+Na" =  {
      #if (max(report$dbase_Na) >= 2) stop();
      vNames <- vector(mode = "character", length = nrow(report));
      if (isTRUE(labels)) {
        vNames[report$dbase_Na == 1 & report$dbase_C == 0] <- "[M+Na]+";
        vNames[
          report$dbase_C  == 0 &
          report$dbase_Na == 0 &
          report$dbase_K  == 0
        ] <- "[M+H]+";
      }
      return(list(
        affnum = report$cluster_C,
        affchar = paste("Cluster", report$cluster_C),
        name = vNames
      ));
    },
    "C+K" = {
      #if (max(report$dbase_K) >= 2) stop();
      vNames <- vector(mode = "character", length = nrow(report));
      if (isTRUE(labels)) {
        vNames[report$dbase_K == 1 & report$dbase_C == 0] <- "[M+K]+";
        vNames[
          report$dbase_C == 0 &
          report$dbase_Na == 0 &
          report$dbase_K == 0
        ] <- "[M+H]+";
      }
      return(list(
        affnum = report$cluster_C,
        affchar = paste("Cluster", report$cluster_C),
        name = vNames
      ));
    },
    "C+Na+K" = {
      #if (max(report$dbase_K) >= 2) stop();
      vNames <- vector(mode = "character", length = nrow(report));
      if (isTRUE(labels)) {
        vNames[report$dbase_Na == 1 & report$dbase_C == 0] <- "[M+Na]+";
        vNames[report$dbase_K == 1 & report$dbase_C == 0] <- "[M+K]+";
        vNames[
         report$dbase_C == 0 &
         report$dbase_Na == 0 &
         report$dbase_K == 0
       ] <- "[M+H]+";
      }
      return(list(
        affnum = report$cluster_C,
        affchar = paste("Cluster", report$cluster_C),
        name = vNames
      ));
    },
    "group" = {
      #if (max(report$dbase_K) >= 2) stop();
      vNames <- vector(mode = "character", length = nrow(report));
      if (isTRUE(labels)) {
        vNames[report$dbase_Na == 1 & report$dbase_C == 0] <- "[M+Na]+";
        vNames[report$dbase_K == 1 & report$dbase_C == 0] <- "[M+K]+";
        vNames[
          report$dbase_C == 0 &
            report$dbase_Na == 0 &
            report$dbase_K == 0
          ] <- "[M+H]+";
      }
      vSubstanceGroup <- group_cluster(dfClusters = data.frame(
        CH2 = report$cluster_CH2,
        C = report$cluster_C,
        Na = report$cluster_Na,
        K = report$cluster_K
      ))
      return(list(
        affnum = vSubstanceGroup,
        affchar = paste("Cluster", vSubstanceGroup),
        name = vNames
      ));
    },
    {

    }
  );
}

# Group matching clusters to larger clusters
group_cluster <- function(dfClusters) {
  vClusterStat <- dfClusters[, 1];
  for (iCol in 2:ncol(dfClusters)) {
    vClustersI <- sort(unique(dfClusters[, iCol]));

    for (ii in 1:length(vClusterStat)) {
      vbSame <- vClusterStat == vClusterStat[[ii]];
      for (cr in dfClusters[vbSame, iCol]) {
        vClusterStat[dfClusters[, iCol] == cr] <- vClusterStat[[ii]];
      }
    }
  }
  ic <- 1;
  for (ii in min(vClusterStat):max(vClusterStat)) {
    if (any(ii == vClusterStat)){
      vClusterStat[vClusterStat == ii] <-ic;
      ic <- ic + 1;
    }
  }
  return(vClusterStat);
}
