#' Load saved R object of find_cluster();
#'
#' @param file Path to rds file
#' @return returns a R object
#' @export
load_report <- function(file) {
  if (!file.exists(file)) stop("ERROR: File desn't exist!\n");
  lReport <- base::readRDS(file = file);
  if (check_report(lReport)) {
    return(lReport);
  } else {
    return(FALSE);
  }
}

#' Save R object of find_cluster()
#'
#' @param report R object of find_cluster
#' @param file Path where file is saved to
#' @return No return
#'
#' @export
save_report <- function(report = NA, file = "") {
  if (is.na(report)) return(FALSE);
  if (file == "") file <- rstudioapi::selectFile(caption = "Save as R data file", existing = FALSE, filter = "RDS (*.rds)")
  if (check_report(report)) {
    saveRDS(object = report, file = file);
  } else {
    cat("ERROR: Invalid object!\n");
  }
}

#' Export R object of find_cluster()
#'
#' Export result to a excel sheet
#'
#' @param report R object of find_cluster
#' @param export_path Path where file is saved to
#' @param export_name Name of exported file
#' @param plot_selection Select plots that have to be exported, Possible are Spectrum,
#' Spectrum_smoothed, Spectrum_Peaks, CH2, C, Na, K, substance. If more plots are of interest,
#' pass a vector c("CH2", "C"). If all are needed, pass "all".
#' @return True
#'
#' @export
export_report <- function(report = NA, export_path = "./", export_name = "none", plot_selection = c("Spectrum_smoothed", "substance")) {
  if (any(is.na(report))) {return(FALSE);}
  s_xlsm_template <- system.file("inst", "data", "template.xlsm", package="lpcfind");
  if (s_xlsm_template == "") {return(FALSE);}
  wb <- xlsx::loadWorkbook(file = s_xlsm_template);

  xlsx::removeSheet(wb, sheetName="Search-Data");
  dataSheet <- xlsx::createSheet(wb, sheetName="Search-Data");

  xlsx::addDataFrame(report$DBsearch, dataSheet)


  # substance cluster -----
  dfNames <- merge(x = report$Report, y = report$DBsearch, all.x = TRUE);
  vNames <- vector(length = nrow(report$Report));
  iUniqueMaxIntMaxx <- unique(dfNames$maxIntMass);

  # Substance Names
  for (ii in 1:length(iUniqueMaxIntMaxx)) {
    vNamesStep <- as.character(dfNames[dfNames$maxIntMass == iUniqueMaxIntMaxx[ii], "Name"]);
    vNamesStep <- vNamesStep[!is.na(vNamesStep)]; # Remove NA
    vNames[ii] <-  paste(vNamesStep, sep = "", collapse = "; ")
  }

  #substance in Legend
  lColTex <- unify_cluster(report = report$Report, method = "group");
  mLegend <- matrix(
    data = c(lColTex$affchar, vNames),
    ncol = 2
  );

  for (nm in unique(mLegend[, 1])) {
    if (length(mLegend[mLegend[, 1] == nm & mLegend[, 2] != "", 1]) == 0) {
      mLegend[mLegend[, 1] == nm, 2] <- mLegend[mLegend[, 1] == nm, 1];
    } else {
      mLegend[mLegend[, 1] == nm, 2] <- paste(mLegend[mLegend[, 1] == nm & mLegend[, 2] != "", 2], sep = "; ", collapse = "; ");
    }
  }

  dfReport <- report$Report;
  dfReport$cluster <- mLegend[, 2];

  xlsx::removeSheet(wb, sheetName="Spectrum-Data");
  dataSheet <- xlsx::createSheet(wb, sheetName="Spectrum-Data");

  xlsx::addDataFrame(dfReport, dataSheet)

  view_report(report, selection = plot_selection, output = "file", export_path = export_path, export_name = export_name);
  #htmlwidgets::saveWidget(obj_Plot, paste(export_path, "/", export_name, ".html", sep = ""))
  xlsx::saveWorkbook(wb, paste(export_path, "/", export_name, ".xlsm", sep = ""));
  return(TRUE);
}

#' Batch export from a batch_processing() return
#'
#' If you want to save the results of the batch_processing() function use this function
#'
#' @param batch_obj By batch_processing() returned object
#' @param export_path Path where report is stored
#' @param plot_selection Vector with plots that should be stored, see export_report()
#' @return TRUE
#'
#' @export
batch_export <- function(batch_obj = NA, export_path = "./", plot_selection = c("Spectrum_smoothed", "substance")) {
  for (obj in batch_obj) {
    s_name_dir <- tools::file_path_sans_ext(basename(obj$path))
    if (length(obj$data$Report) == 0) {
      cat("No cluster found in", s_name_dir, "\n");
      next;
    }
    s_path_new <- paste(export_path, "/", s_name_dir, "/", sep = "");
    dir.create(s_path_new);
    # Export each element
    export_report(
      report         = obj$data,
      export_path    = s_path_new,
      export_name    = s_name_dir,
      plot_selection = plot_selection);
  }
  return(TRUE);
}

#' View results
#'
#' Currently only plotly plots are possible.
#'
#' @param report R object of find_cluster()
#' @param output Method of output. Possible is \code{raw}
#' @return Return depends of output.
#' \code{raw} results in a list of plotly plots
#' @importFrom dplyr "%>%"
#'
#' @export
view_report <- function(report = NULL, selection = "all", output = "raw", export_path = "./", export_name = "none") {
  if(is.null(report)) return(TRUE)
  get_plots <- function(report, selection) {
    lPlots <- list();

    # Raw Spectrum -----
    lPlots$Spectrum <- plotly::plot_ly( # Spectrum
      x = ~report$Spectrum$mass,
      y = ~report$Spectrum$intensity,
      mode = "lines"
    )%>%
      plotly::layout(title = 'Spectrum',
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # Smoothed Spectrum -----
    lPlots$Spectrum_smoothed <- plotly::plot_ly( # Spectrum
      x = ~report$Spectrum_smoothed$mass,
      y = ~report$Spectrum_smoothed$intensity,
      mode = "lines"
    )%>%
      plotly::layout(title = 'Smoothed spectrum',
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # Discrete spectrum -----
    lPlots$Spectrum_Peaks <- plotly::plot_ly(
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      name= "C",
      type = "bar"
    ) %>%
      plotly::layout(title = 'Discrete spectrum',
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # CH2 pattern -----
    lPlots$CH2 <- plotly::plot_ly(
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      color = ~as.character(paste("C",report$Report$cluster_CH2)),
      name= ~as.character(paste("C", report$Report$cluster_CH2)),
      type = "bar"
    ) %>%
      plotly::layout(title = 'Cluster spectrum [CH2 Massdiference]',
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # dC -----
    lPlots$C <- plotly::plot_ly(
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      color = ~as.character(paste("C", report$Report$cluster_C)),
      name= ~as.character(paste("C", report$Report$cluster_C)),
      type = "bar"
    ) %>%
      plotly::layout(title = "Isotope Pattern [C13]",
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # Na Cluster -----
    #getPalette <- colorRampPalette(brewer.pal(9, "Set1"))
    lColTex <- unify_cluster(report = report$Report, method = "C+Na");
    #colourCount <- length(unique(lColTex$affchar))
    lPlots$Na <- plotly::plot_ly( # Na
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      color = ~lColTex$affchar,# ~as.character(paste("C", report$cluster_Na)),
      name = ~lColTex$affchar,
      type = "bar"#,
      #palette = getPalette(colourCount)
    ) %>%
      plotly::add_trace(
        x = ~report$Report$maxIntMass,
        y = ~report$Report$intensity,
        text = lColTex$name,
        mode = "text",
        type = "scatter",
        textposition = "top center",
        textfont = list(color= "#000000", size=10),
        showlegend = FALSE
      ) %>%
      plotly::layout(title = "Isotope Pattern [C13 + Sodium Adducts]",
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # K Cluster -----
    lColTex <- unify_cluster(report = report$Report, method = "C+K");
    #colourCount <- length(unique(lColTex$affchar))
    lPlots$K <- plotly::plot_ly( # K
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      color = ~lColTex$affchar,# ~as.character(paste("C", report$Report$cluster_Na)),
      name = ~lColTex$affchar,
      type = "bar"#,
      #palette = getPalette(colourCount)
    ) %>%
      plotly::add_trace(
        x = ~report$Report$maxIntMass,
        y = ~report$Report$intensity,
        text = lColTex$name,
        mode = "text",
        type = "scatter",
        textposition = "top center",
        textfont = list(color= "#000000", size=10),
        showlegend = FALSE
      )%>%
      plotly::layout(title = "Isotope Pattern [C13 + Potassium Adducts]",
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );

    # substance cluster -----
    dfNames <- merge(x = report$Report, y = report$DBsearch, all.x = TRUE);
    vNames <- vector(length = nrow(report$Report));
    iUniqueMaxIntMaxx <- unique(dfNames$maxIntMass);

    for (ii in 1:length(iUniqueMaxIntMaxx)) {
      vNamesStep <- as.character(dfNames[dfNames$maxIntMass == iUniqueMaxIntMaxx[ii], "Name"]);
      vNamesStep <- vNamesStep[!is.na(vNamesStep)]; # Remove NA
      vNames[ii] <-  paste(vNamesStep, sep = "", collapse = "; ")
    }

    #substance in Legend
    lColTex <- unify_cluster(report = report$Report, method = "group");
    mLegend <- matrix(
      data = c(lColTex$affchar, vNames),
      ncol = 2
    );
    for (nm in unique(mLegend[, 1])) {
      if (length(mLegend[mLegend[, 1] == nm & mLegend[, 2] != "", 1]) == 0) {
        mLegend[mLegend[, 1] == nm, 2] <- mLegend[mLegend[, 1] == nm, 1];
      } else {
        mLegend[mLegend[, 1] == nm, 2] <- paste(mLegend[mLegend[, 1] == nm & mLegend[, 2] != "", 2], sep = "; ", collapse = "; ");
      }
    }
    #colourCount <- length(unique(lColTex$affchar))
    lPlots$substance <- plotly::plot_ly( # K
      x = ~report$Report$maxIntMass,
      y = ~report$Report$intensity,
      color = ~lColTex$affchar,# ~as.character(paste("C", report$Report$cluster_Na)),
      name = ~mLegend[, 2], #~lColTex$affchar,
      type = "bar",
      showlegend = TRUE
      #palette = getPalette(colourCount)
    ) %>%
      plotly::add_trace(
        x = ~report$Report$maxIntMass,
        y = ~report$Report$intensity,
        text = trimws(vNames), #text = trimws(paste(lColTex$name, vNames, sep = " ")),
        mode = "text",
        type = "scatter",
        textposition = "top center",
        textfont = list(color= "#000000", size=10),
        showlegend = FALSE
      )%>%
      plotly::layout(title = "Isotope Pattern [Substance class]",
             yaxis = list(title = "I"),
             xaxis = list(title = "m/z")
      );
    return(lPlots[selection]);
  }

  if (all(selection == "all")) {
    vs_selection <- c(
      "Spectrum", "Spectrum_smoothed", "Spectrum_Peaks", "CH2", "C", "Na", "K", "substance"
    );
  } else {
    vs_selection <- selection;
  }

  switch (output,
    "raw" = {
      # Create plots -----
      lPlots <- get_plots(report = report, selection = vs_selection);

      # Post processing -----
      #if (isTRUE(view)) lPlots;
      return(lPlots);
    },
    "file" = {
      lPlots <- get_plots(report = report, selection = vs_selection);
      for (ii in 1:length(lPlots)) {
        htmlwidgets::saveWidget(lPlots[[ii]],
          normalizePath(
            paste(export_path, "/", export_name, "-", names(lPlots[ii]), ".html", sep = ""),
          mustWork = FALSE)
        );
      }
    },
    {
      cat("ERROR: Invalid output!\n");
    }
  )
}

#' Explore Report
#'
#' Interacttive explore the Export object
#'
#' @param report Report Object
#'
#' @return Status (True by default)
#'
#' @export
explore_report <- function(report) {
s_help_text <-
"
explore_report: Interacttive explore a export object

help:         Display this helptext
quit:         Quit this function
list:         List all evaluated datasets
set_plots:    View types of plots
view_Plots:   View a dataset

"
  dfPlots <- data.frame(
    Plot = c("Spectrum", "Spectrum_smoothed", "Spectrum_Peaks", "CH2", "C",   "Na",  "K",   "substance"),
    View = c(FALSE,      TRUE,                FALSE,            FALSE, FALSE, FALSE, FALSE, TRUE       )
  );
  dfData = data.frame(Number = 1:length(report), Path = vector(length = length(report)));
  for (i in 1:length(report)) dfData$Path = report[[i]]$path;

  cat(s_help_text);

  repeat {
    s_command <- readline(prompt = "[explore]>");
    switch(s_command,
      "help" = {
        cat(s_help_text);
      },
      "quit" = {
        cat("Quit.")
        break;
      },
      "list" = {
        print(dfData);
      },
      "set_plots" = {
        print(dfPlots);
        cat("Type numbers of plots separated by , that should be viewed or unviewed. If nothing should be changed, keep it empty")
        v_modify <- readline(prompt = "[explore:set_plot]>");
        if (v_modify == "") next; # If nothing typed
        vi_selected <- eval(parse(text=paste("c(",v_modify, ")", sep = ""))); # Evaluate
        if (any(vi_selected > nrow(dfPlots))) {
          cat("Input out of range.");
          next;
        } else if (any(!is.numeric(vi_selected))) {
          cat("Wrong type.");
          next;
        }
        dfPlots$View[vi_selected] <- !dfPlots$View[vi_selected];
      },
      "view_plots" = {
        print(dfData);
        cat("Select a dataset by number:")
        s_number <- readline(prompt = "[explore:view_plots]>")
        if (s_number == "") next;
        i_number <- as.numeric(s_number);
        #eval(view_report(report = report[[i_number]]$data, selection = as.character(dfPlots$Plot[dfPlots$View]), output = "raw"), envir = .GlobalEnv);
        REP_PLOTLY <<- view_report(report = report[[i_number]]$data, selection = as.character(dfPlots$Plot[dfPlots$View]), output = "raw");
        eval(REP_PLOTLY, envir = .GlobalEnv);
      },
      {
        cat("Unknown command");
      }
    );
  }
  return(TRUE);
}


#' Check, if a R object is a valid find_cluster() output
#'
#' @param report a find_cluster() return
#' @return \code{TRUE} or \code{FALSE}.
#'
check_report <- function(report) {
  l_return <-list(
    general = FALSE,
    Spectrum = FALSE,
    Spectrum_smoothed = FALSE,
    Spectrum_Peaks = FALSE,
    Report = FALSE,
    DBsearch = FALSE,
    variant = FALSE
  )
  if ( !
    all(names(report) == c(
      "Spectrum", "Spectrum_smoothed, Spectrum_Peaks", "Report", "DBsearch", "variant"
    ))
  ) return(FALSE);

  return(TRUE);
}
