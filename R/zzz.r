# On load
.onLoad <- function(libname, pkgname) {
  # Init cache path
  #R.cache::setCacheRootPath(path = paste(
  #  "~/.lpcfind-cache-", format(Sys.time(), "%Y%m%d_%H-%M-%S"),
  #sep = ""));
  R.cache::setCacheRootPath(path = "~/.lpcfind-cache");
  cache_dir <- check_cache();
  #cat(cache_dir)
}
.onAttach <- function(libname, pkgname) {
  #dbinfo <- update_databases();
  dbinfo <- TRUE;
  if (! isFALSE(dbinfo)) dbinfo <- download_database.bioinfo_cristal();
  if (! isFALSE(dbinfo)) save_substances();
  packageStartupMessage(paste(
    "Welcome to ", pkgname,
    ", written by Marco Grimmeissen and Volker Hoefer.\n",
    "\n",
    "Cache: ", lc_config(key = "cache.dir"), sep = ""
  ));
  if (isFALSE(dbinfo)) warning("Warning: Building of substance database failed!")
}
.onUnload <- function(libname, pkgname) {
  # Remove cache dir
  if (dir.exists(lc_config(key = "cache.dir"))) unlink(lc_config(key = "cache.dir"), recursive = TRUE);
}
