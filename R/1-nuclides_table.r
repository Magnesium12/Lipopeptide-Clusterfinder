# data.frame of Masses, from:
# https://de.wikipedia.org/wiki/Liste_der_Isotope/

isotope <- function(p = 1, n = 0, m = 0, a = 0, hl = Inf) {
  if (is.infinite(hl)) {
    bRadioactive <- FALSE;
  } else {
    bRadioactive <- TRUE;
  }
  return(list(
    Symbol = ELEMENTS[p, "Symbol"],
    NameEN = ELEMENTS[p, "EN"],
    NameDE = ELEMENTS[p, "DE"],
    Protons = p,
    Neutrons = n,
    Mass = m,
    NominalMass = p + n,
    Abundance = a,
    Halflive = hl,
    Radioactive = bRadioactive
  ));
}
ELEMENTS <- data.frame(
  Symbol = c(
    "H",                                                                                                                                                                                      "He",
    "Li", "Be",                                                                                                                                                 "B",  "C",  "N",  "O",  "F",  "Ne",
    "Na", "Mg",                                                                                                                                                 "Al", "Si", "P",  "S",  "Cl", "Ar",
    "K",  "Ca", "Sc",                                                                                     "Ti", "V",  "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr",
    "Rb", "Sr", "Y",                                                                                      "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I",  "Xe",
    "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W",  "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Fr", "Ra", "Ac", "Th", "Pa", "U",  "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"
  ),
  DE = c(
    "Wasserstoff", "Helium",
    "Lithium", "Beryllium", "Bor", "Kohlenstoff", "Stickstoff", "Sauerstoff", "Fluor", "Neon",
    "Natrium", "Magnesium", "Aluminium", "Silicium", "Phosphor3", "Schwefel", "Chlor", "Argon",
    "Kalium", "Calcium", "Scandium", "Titan", "Vanadium", "Chrom", "Mangan", "Eisen", "Cobalt", "Nickel", "Kupfer", "Zink", "Gallium", "Germanium", "Arsen", "Selen", "Brom", "Krypton",
    "Rubidium", "Strontium", "Yttrium", "Zirconium", "Niob", "Molybdaen", "Technetium", "Ruthenium", "Rhodium", "Palladium", "Silber", "Cadmium", "Indium", "Zinn", "Antimon", "Tellur", "Iod", "Xenon",
    "Caesium", "Barium", "Lanthan", "Cer", "Praseodym", "Neodym", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium", "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium", "Hafnium", "Tantal", "Wolfram", "Rhenium", "Osmium", "Iridium", "Platin", "Gold", "Quecksilber", "Thallium", "Blei", "Bismut", "Polonium", "Astat", "Radon",
    "Francium", "Radium", "Actinium", "Thorium", "Protactinium", "Uran", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium", "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium", "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium", "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium", "Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tenness", "Oganesson"
  ),
  EN = c(
    "Hydrogen", "Helium",
    "Lithium", "Beryllium", "Boron", "Carbon", "Nitrogen", "Oxygen", "Fluorine", "Neon",
    "Sodium", "Magnesium", "Aluminum,", "Silicon", "Phosphorus", "Sulfur", "Chlorine", "Argon",
    "Potassium", "Calcium", "Scandium", "Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt", "Nickel", "Copper", "Zinc", "Gallium", "Germanium", "Arsenic", "Selenium", "Bromine", "Krypton",
    "Rubidium", "Strontium", "Yttrium", "Zirconium", "Niobium", "Molybdenum", "Technetium", "Ruthenium", "Rhodium", "Palladium", "Silver", "Cadmium", "Indium", "Tin", "Antimony", "Tellurium", "Iodine", "Xenon",
    "Cesium", "Barium", "Lanthanum", "Cerium", "Praseodymium", "Neodymium", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium", "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium", "Hafnium", "Tantalum", "Tungsten", "Rhenium", "Osmium", "Iridium", "Platinum", "Gold", "Mercury", "Thallium", "Lead", "Bismuth", "Polonium", "Astatine", "Radon",
    "Francium", "Radium", "Actinium", "Thorium", "Protactinium", "Uranium", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium", "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium", "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium", "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium", "Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tennessine", "Oganesson"
  )
);
ISOTOPES <- rbind.data.frame(
  isotope(p =   1, n =   0, m = 1.00782503224,  a = 99.985/100),
  isotope(p =   1, n =   1, m = 2.01410177812,  a = 0.015/100),
  isotope(p =   1, n =   2, m = 3.0160492779,   a = 0/100, hl = 12.33 * TIME$year),
  isotope(p =   6, n =   6, m = 12.0000,        a = 98.90/100),
  isotope(p =   6, n =   7, m = 13.00335483507, a = 1.10/100),
  isotope(p =   7, n =   7, m = 14.00307400443, a = 99.634/100),
  isotope(p =   7, n =   8, m = 15.00010889889, a = 0.366/100),
  isotope(p =   8, n =   8, m = 15.99491461957, a =	99.762/100),
  isotope(p =   8, n =   9, m = 16.99913175651, a =	0.038/100),
  isotope(p =   8, n =  10, m = 17.99915961287, a =	0.200/100),
  isotope(p =  11, n =  12, m = 22.9897692820,  a = 100/100),
  isotope(p =  15, n =  16, m = 30.97376199842, a = 100/100),
  isotope(p =  16, n =  16, m = 31.9720711744,  a = 94.99/100),
  isotope(p =  16, n =  17, m = 32.9714589098,  a = 0.75/100),
  isotope(p =  16, n =  18, m = 33.967867004,   a = 4.25/100),
  isotope(p =  16, n =  20, m = 35.96708071,    a = 0.01/100),
  isotope(p =  17, n =  18, m = 34.968852682,   a = 75.76/100),
  isotope(p =  17, n =  20, m = 36.965902602,   a = 24.24/100),
  isotope(p =  19, n =  20, m = 38.9637064864,  a = 93.3581/100),
  isotope(p =  19, n =  21, m = 39.963998167,   a = 0.0117/100, hl = 1.277 * 10^9 * TIME$year),
  isotope(p =  19, n =  22, m = 40.9618252579,  a = 6.7302/100),
  isotope(p =  35, n =  44, m = 78.9183376,     a = 50.69/100),
  isotope(p =  35, n =  46, m = 80.9162897,     a = 49.31/100),
  isotope(p =  53, n =  74, m = 126.90447,      a = 100/100)
);

rownames(ISOTOPES) <- paste(ISOTOPES[, "Symbol"], ISOTOPES[, "NominalMass"], sep = "");
