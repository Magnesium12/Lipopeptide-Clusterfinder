#' batch_processing: Evaluate multiple datasets
#'
#' If you have multiple measured files you want to analyse use this function.
#'
#' @param path Path of files. TXT amd mzXML are currently supported.
#' @param counter Counter of evaluated files. Must not be defined!
#' @param ... Parameter for find_cluster().
#'
#' @return List of evaluated datasets. Each element of the list contains a list with the path of the origin file and the returned dataset of find_cluster().
#'
#' @export
batch_processing <- function(path = "./", counter = NA, ...) {
  if (is.na(counter)) {
    iCount <- 0;
  } else {
    iCount <- counter;
  }
  RES <- list();
  for (obj in dir(path)) {
    s_file_path <- paste(path, obj, sep = "/");

    if (dir.exists(s_file_path)) {
      # Do it Recursive
      lSubBatch <- batch_processing(path = s_file_path, counter = iCount, ...);
      RES <- c(RES, lSubBatch$data);
      iCount <- lSubBatch$counter;
    } else if (file.exists(s_file_path)) {
      switch (tolower(tools::file_ext(s_file_path)),
        "mzxml" = {
          obj_spc <- readMzXmlData::readMzXmlFile(s_file_path);
          dfSpectrum <- as.data.frame(obj_spc$spectrum);
          iCount <- iCount + 1;
        },
        "txt"   = {
          obj_spc <- utils::read.table(s_file_path);
          dfSpectrum <- obj_spc;
          colnames(dfSpectrum) <- c("mass", "intensity");
          iCount <- iCount + 1;
        }
      );
      cat(paste("Evaluating dataset ", iCount, "\n", sep = ""));
      RES[[length(RES)+1]] <- list(
        path = s_file_path,
        data = find_cluster(dfSpectrum = dfSpectrum, ...)
      );
    }
  }
  if (is.na(counter)) {
    return(RES);
  } else {
    return(list(counter = iCount, data = RES));
  }
}
