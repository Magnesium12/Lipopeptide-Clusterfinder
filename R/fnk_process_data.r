#' Find lipide patterns (lipopeptide_alkyle_patterns())
#'
#' Finds in mass specra 14 Da steps between two peaks typical for lipopeptide cluster
#'
#' coming soon
#'
#' @param Peaks data.frame with a table of identyfied peaks. Creatid by find_peaks()
#' @param number_of_methylene Minimum number of CH_2 groups a pattern must have.
#' If not, the pattern isn't saved
#' @param uncertainty Uncertainty in dalton for neighbor peak candidates.
#' If a peak is in a range n*(14Da+-u) it is a candidate.
#' @param intRatio Hight relative to the selected peak a peak candidate must have
#' to be selected.
#' @param stepMass Mass difference between two species.
#' @return List with data.frame and stat
#'
lipopeptide_alkyle_patterns <- function(Peaks, number_of_methylene = 3,
  uncertainty = 0.05, intRatio = 0.05, stepMass = 14.01565
){
  dfPeaks <- Peaks;
  step_mass <- stepMass;
  dfCluster <- data.frame(
    leftBorder = NA,
    rightBorder = NA,
    maxIntMass = NA,
    intensity = NA,
    cluster = NA,
    dbase = NA
  );
  #dfPeaks <- dfPeaks[order(dfPeaks$Intensity),];
  # Methods -----

  # Do a mass step realtive from a selected peak (letsStep())
  #
  # @param dfPeaks data.frame of all found peaks
  # @param Max Column index of Max Peak
  # @param step direction and width of a step. Must be an integer. If negative,
  # it steps to lower m/z if positive it steps to higher m/z
  # @param stepWidth width between steps. Normaly it is the mass of CH_2
  # @param uncertainty Uncertainty in Da for the next peak
  # @return Returns a list with stat values and a data.frame with potential
  # fitting peaks for the selected peak in step direction and width.
  #
  letsStep <- function(dfPeaks, Max, step, stepWidth, uncertainty) {
    rEstPeak <- dfPeaks[Max, "maxIntMass"] + step*stepWidth;
    #lc_log("Step from ", dfPeaks[Max, "maxIntMass"]," to: ", rEstPeak, "\n")
    vbFittingPeaks <- ((dfPeaks$maxIntMass <= rEstPeak + uncertainty) & (dfPeaks$maxIntMass >= rEstPeak - uncertainty));
    dfFittingPeaks <- dfPeaks[
      vbFittingPeaks,
    ];
    if (all(!vbFittingPeaks)) { # (length(vbFittingPeaks[vbFittingPeaks == TRUE]) == 0)
      # No peaks found
      return(list(stat = FALSE, log = 1));
    } else if (sum(vbFittingPeaks) == 1) { # Nice case
      # One Peak found in selected range: good
      if (any(!is.na(dfFittingPeaks[1, ])) && (dfFittingPeaks[1, "intensity"] >= rIntTresh)) { #Internsity enought
        # No NA, good
        return(list(
          stat = TRUE,
          log = 0,
          fit = dfFittingPeaks
          #dfPeaks = dfPeaks[!vbFittingPeaks,] # Remove
        ));
      } else {
        # data.frame contains NA, bad, throw error
        return(list(stat = FALSE, log = 2));
      }
    } else {
      # More than one Peak, bad
      return(list(stat = FALSE, log = 3));
    }
  }
  # Prepare Data, sorting by m/z -----
  dfPeaks <- dfPeaks[order(dfPeaks$maxIntMass), ];

  # Detect Cluster -----
  iCluster <- 1;
  lc_log("14Da Step\n");
  iStart <- nrow(dfPeaks);
  pb <- lc_progress_bar_init(min = 0, max = nrow(dfPeaks), style = lc_config(key = "progress_bar.style") , label = "14Da Step");
  while (nrow(dfPeaks) > 0) {
    # Peak with max intensity
    iMax <- order(dfPeaks$intensity, decreasing = TRUE)[1];
    if (any(is.na(dfPeaks[iMax,]))) next();
    # Limit of intensity for next peak
    rIntTresh <- dfPeaks[iMax, "intensity"] * intRatio;
    # Max Peak in growing 14Da cluster
    dfProtoCluster <- dfPeaks[iMax,]

    for (ilr in c(-1, 1)) { # go left and right of max peak
      ic <- 1; # Number of steps in selected direction
      repeat {
        variantTmp <- letsStep( #Result of letsStep
          dfPeaks = dfPeaks,
          Max = iMax,
          step = ilr*ic,
          stepWidth = step_mass,
          uncertainty = uncertainty
        );
        if (isFALSE(variantTmp$stat)) {
          # If no peak could be found, break()
          break();
        } else {
          # Peaks found -> peaks are attached to dfProtoCluster
          dfProtoCluster <- rbind(dfProtoCluster, variantTmp$fit);
          ic <- ic + 1; # next m/z
        }
      }
    }
    # Clean

    # Remove found peaks from dfPeaks
    dfPeaks <- dplyr::setdiff(dfPeaks, dfProtoCluster);

    # Is ProtoCluster a real Cluster? ----
    #Check for number of entrys
    if (nrow(dfProtoCluster) >= number_of_methylene) {
      # vbase: For identifying the basepeak
      vdbase <- 1:nrow(dfProtoCluster) - 1;
      #vdbase <- vdbase[order(dfProtoCluster$maxIntMass)];
      dfProtoCluster <- dfProtoCluster[order(dfProtoCluster$maxIntMass),];
      #lc_log("Bind", "\n");
      dfCluster <- rbind(dfCluster,
                         cbind(dfProtoCluster, cluster = iCluster, dbase = vdbase)
      );
      # If Cluster found, number increases
      iCluster <- iCluster + 1;
    }
    lc_progress_bar_step(pb, iStart - nrow(dfPeaks));
  }
  lc_progress_bar_close(pb);
  lc_log("\nDone\n");

  dfCluster <- dfCluster[-1,]; # Remove first column of data.frame, contains NA
  # Order by m/z
  dfCluster <- dfCluster[order(dfCluster$maxIntMass),];

  # No Cluster found
  if (nrow(dfCluster) == 0) {
    return(list(
      stat = 1,
      peak_table = dfCluster
    ));
  }
  return(list(
    stat = 0,
    peak_table = dfCluster
  ));
}

#' Identify cluster by isotope patterns
#'
#' Unifys cluster with matching isotope pattern and a characteristic shift.
#'
#' @param dfPreCluster data.frame countaining 14Da cluster
#' @param uncertainty Uncertainty in Da
#' @param thresh Limit of variance between clusters. If larger than selected
#' value, clusters get not unified.
#' @param stepMass Mass shift between clusters. 1Da for C-pattern, 22Da for Na Cluster
#' @return A list containing a stat and a data.frame
#'
isotope_patterns <- function(dfPreCluster,  # @param number_of_isotopes Number of isotope peaks necessary to be a cluster
  uncertainty        = 0.05,
  #number_of_isotopes = 0,    #    %=%
  thresh             = 0.01,
  stepMass           = 1.003355
){
  # Prepare Data, sorting by m/z -----
  dfPreCluster <- dfPreCluster[order(dfPreCluster$maxIntMass),];
  # Duplicate Data
  dfIsotopePattern <- dfPreCluster;
  # Bins of all clusters
  vClustersBins <- unique(sort(dfPreCluster$cluster));
  # Isotope step
  rStep <- stepMass;

  # List of 1Da step clusters, gets populated while next loop
  lClusterSame <- list();

  # Methods -----
  # Compare the clusters
  #
  # Compare two clusters by norming and calculating the variance
  # @param dfCluster1 Cluster 1
  # @param dfCluster2 Cluster 2
  # @return A list with variance and shift in m/z
  compare_cluster <- function(dfCluster1, dfCluster2) {
    # 0. Sort:
    dfCluster1 <- dfCluster1[order(dfCluster1$maxIntMass),];
    dfCluster2 <- dfCluster2[order(dfCluster2$maxIntMass),];

    # 1. Compare
    iMaxInt1 <- order(dfCluster1$intensity, decreasing = TRUE)[1];
    vRange1 <- c(iMaxInt1 - 1, nrow(dfCluster1) - iMaxInt1);
    iMaxInt2 <- order(dfCluster2$intensity, decreasing = TRUE)[1];
    vRange2 <- c(iMaxInt2 - 1, nrow(dfCluster2) - iMaxInt2);
    vRange <- c(min(vRange1[1], vRange2[1]), min(vRange1[2], vRange2[2]))

    # 2. Cut
    dfCluster1 <- dfCluster1[(iMaxInt1-vRange[1]):(iMaxInt1+vRange[2]),];
    iMaxInt1 <- order(dfCluster1$intensity, decreasing = TRUE)[1];
    dfCluster2 <- dfCluster2[(iMaxInt2-vRange[1]):(iMaxInt2+vRange[2]),];
    iMaxInt2 <- order(dfCluster2$intensity, decreasing = TRUE)[1];

    # 3. Norm
    dfCluster1$intensity <- dfCluster1$intensity / sum(dfCluster1$intensity);
    dfCluster2$intensity <- dfCluster2$intensity / sum(dfCluster2$intensity);

    #4. Calc
    rDeltaI <- sum((dfCluster1$intensity - dfCluster2$intensity)^2);
    if (is.na(abs(dfCluster1[iMaxInt1, "maxIntMass"] - dfCluster2[iMaxInt2, "maxIntMass"]))) {
      cat("ERROR! data.frame out of range!\n")
      stop();
    }
    return(list(
      var = rDeltaI,
      deltaMZ = abs(dfCluster1[iMaxInt1, "maxIntMass"] - dfCluster2[iMaxInt2, "maxIntMass"])
    ));
  }

  # Identify Associated Cluster
  #
  # Find one or two associated cluster to the cluster of the max peak.
  # Scans only one to left and one to right.
  #
  # @param dfIsotopePattern data.frame with 14Da identified peaks
  # @param rStep step width
  # @param uncertainty Uncertainty in Da
  # @return A vector containing cluster numbers of pattern matching clusters.
  # First entry ist the entry of the max peak
  getAssociatedCluster <- function(
    dfIsotopePattern = dfIsotopePattern,
    rStep = rStep,
    uncertainty = uncertainty
  ){
    ## Cluster with max intensity
    iMaxIntCluster <- dfIsotopePattern[order(dfIsotopePattern$intensity, decreasing = TRUE)[1],]$cluster;
    # Dataframe of Cluster
    dfMaxIntCluster <- dfIsotopePattern[dfIsotopePattern$cluster == iMaxIntCluster,];
    # the other cluster
    dfOtherCluster <- dfIsotopePattern[!dfIsotopePattern$cluster == iMaxIntCluster,];
    # Vector of cluster
    vCluster <- vector();

    for (ilr in c(-1, 1)) { # look left, right
      rEstPeak <- dfMaxIntCluster[order(dfMaxIntCluster$intensity, decreasing = TRUE)[1], "maxIntMass"] + ilr*rStep;
      vbFittingPeaks <- (
        (dfOtherCluster$maxIntMass <= rEstPeak + uncertainty) &
        (dfOtherCluster$maxIntMass >= rEstPeak - uncertainty)
      );
      dfFittingPeaks <- dfOtherCluster[
          vbFittingPeaks,
      ];
      if (any(vbFittingPeaks)) vCluster <- c(vCluster, dfFittingPeaks$cluster)
    }
    vCluster <- unique(vCluster[order(vCluster)]); # Sheeeet
    vCluster <- c(iMaxIntCluster, vCluster); # first entry is highest peak, has to be removed further
    return(vCluster);
  }

  # Scan for isotope patterns -----
  iStart <- nrow(dfIsotopePattern);
  if (iStart == 0) {
    return(list(
      stat = 1,
      peak_table = dfIsotopePattern
    ));
  }
  lc_log("Isotope Pattern detection\n");
  pb <- lc_progress_bar_init(min = 0, max = iStart, style = lc_config(key = "progress_bar.style") , label = "Smoothing");
  repeat{ # every loop calculates the max cluster
    vAssociatedCluster <- vector();
    # returns max cluster and 1-2 associated cluster
    vAssociatedCluster <- getAssociatedCluster(
      dfIsotopePattern = dfIsotopePattern,
      rStep = rStep,
      uncertainty = uncertainty
    );

    # data.frame for intensity ratio criterion
    dfDeltaCluster <- data.frame(
      var = vector(mode = "numeric", length = length(vAssociatedCluster)),
      deltaMZ = vector(mode = "numeric", length = length(vAssociatedCluster))
    );

    # Set first row to zero, because this are the results, if a cluster is
    # compared with itself.
    dfDeltaCluster[1, "var"] <- 0;
    dfDeltaCluster[1, "deltaMZ"] <- 0;

    # Variance between cluster
    if (length(vAssociatedCluster) >= 2) {
      # Have to be longer  or equal than two,
      #because the first is the max cluster itself.
      for (ii in 2:length(vAssociatedCluster)) {
        # Compare all associated cluster with the first/max cluster
        dfDeltaCluster[ii,] <- compare_cluster(
          dfCluster1 = dfIsotopePattern[dfIsotopePattern$cluster == vAssociatedCluster[1],],
          dfCluster2 = dfIsotopePattern[dfIsotopePattern$cluster == vAssociatedCluster[ii],]
        );
      }
    }

    # Check for variance and delta M/Z criterion.
    # If variance is larger than thresh, the cluster gets wasted.
    lClusterSame[[length(lClusterSame) + 1]] <- vAssociatedCluster[(
      dfDeltaCluster$var <= thresh &
      dfDeltaCluster$deltaMZ == 0 | ( # Truncate cluster a with cluster a, IMPORTANT!!!!
        dfDeltaCluster$deltaMZ <= rStep + uncertainty &
        dfDeltaCluster$deltaMZ >= rStep - uncertainty
      )
    )];

    # Remove max cluster from peakt table, repeat with second intensive cluster
    dfIsotopePattern <- dfIsotopePattern[!dfIsotopePattern$cluster == vAssociatedCluster[1],];

    lc_progress_bar_step(pb, iStart - nrow(dfIsotopePattern));
    if (nrow(dfIsotopePattern) == 0) break();
  }
  lc_progress_bar_close(pb);
  lc_log("\nDone\n")

  # Aggregate Cluster -----
  lc_log("Aggregate Cluster\n");
  pb <- lc_progress_bar_init(min = min(vClustersBins), max = max(vClustersBins), style = lc_config(key = "progress_bar.style") , label = "Smoothing");
  for (iCluster in vClustersBins) {
    vStep <- vector();
    vDel <- vector();
    for (ii in 1:length(lClusterSame)) {
      if (any(lClusterSame[[ii]] == iCluster)) {
        vStep <- c(vStep, lClusterSame[[ii]]);
        vDel <- c(vDel, ii);
      }
    }
    for (iDel in vDel) lClusterSame[[iDel]] <- FALSE;
    lClusterSame[[length(lClusterSame)+1]] <- unique(sort(vStep));
    lc_progress_bar_step(pb, iCluster);
  }
  lc_progress_bar_close(pb);
  lc_log("\nDone\n");

  # Sort Groups Decreasing ----
  lClusterSameNew <- list();
  repeat {
    vMin <- c(0, Inf); # Index, Value
    for (ii in 1:length(lClusterSame)) {
      if (isFALSE(lClusterSame[[ii]])) next();
      if (is.na(lClusterSame[[ii]]) || min(lClusterSame[[ii]]) < vMin[2]) {
        vMin <- c(ii, min(lClusterSame[[ii]]));
      }
    }
    if (all(vMin == c(0, Inf))) break(); # Check, if found
    lClusterSameNew[[length(lClusterSameNew) + 1]] <- lClusterSame[[vMin[1]]];
    lClusterSame[[vMin[1]]] <- FALSE;
  }
  lClusterSame <- lClusterSameNew;
  rm(lClusterSameNew);

  # Rebuild the data.frame from Backup -----
  dfIsotopePattern <- dfPreCluster;

  # Create new bins -----
  ic <- 1;
  for (vClusterSame in lClusterSame) {
    #if (isFALSE(vClusterSame)) next();
    for (iCluster in vClusterSame) {
      dfIsotopePattern[dfPreCluster$cluster == iCluster, "cluster"] <- ic;
    }
    ic <- ic + 1;
  }

  # New dbase
  # dbase before
  v_dbase_CH2 <- dfIsotopePattern$dbase;
  dfIsotopePattern$dbase <- 0;
  v_dbase_CH2_unique <- sort(unique(v_dbase_CH2));
  if (!all((v_dbase_CH2_unique == min(v_dbase_CH2_unique):max(v_dbase_CH2_unique)))) {
    cat("ERROR: non continous numbering of alk clusters!");
  }
  for (i_dbase_alk_unique in v_dbase_CH2_unique) {
    vCluster <- sort(unique(dfIsotopePattern[
      v_dbase_CH2 == i_dbase_alk_unique, "cluster"
    ]));
    # numerate cluster of same CH2 vbase
    for (iCluster in vCluster) {
     dfCluster <- dfIsotopePattern[
        dfIsotopePattern$cluster == iCluster &
        v_dbase_CH2 == i_dbase_alk_unique
     ,];
     dfCluster <- dfCluster[order(dfCluster$maxIntMass),]
     dfCluster$dbase <- 1:nrow(dfCluster) - 1;
     dfIsotopePattern[
       dfIsotopePattern$cluster == iCluster &
         v_dbase_CH2 == i_dbase_alk_unique
      ,] <- dfCluster;
   }
  }

  # Order
  dfIsotopePattern <- dfIsotopePattern[order(dfIsotopePattern$maxIntMass),];

  return(list(
    stat = 0,
    peak_table = dfIsotopePattern
  ));
}


