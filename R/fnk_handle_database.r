# For searching a peak in the substances database
search_database <- function(dfSearch,
  uncertancy = 0.05,
  ion = ISOTOPES[ISOTOPES$Symbol == "H" & ISOTOPES$Neutrons == 0, "Mass"],
  db = get_substances()
){
  # Return Value, a data.frame
  #lReturn <- list();
  dfReturn <- as.data.frame(list(dfSearch[1, ], data.frame("matching" = NA), db[1, ])); dfReturn[1, ] <- NA;
  dfCol <- db[1, ]; dfCol[1, ] <- NA;

  lc_log("Search Database\n");
  pb <- lc_progress_bar_init(min = 0, max = nrow(dfSearch), style = lc_config(key = "progress_bar.style") , label = "Smoothing");
  for (iRow in 1:nrow(dfSearch)) {
    # Step trought results
    dfFound <- db[
      db$Monoisotopic.mass >= dfSearch[iRow, "maxIntMass"] - ion - uncertancy &
      db$Monoisotopic.mass <= dfSearch[iRow, "maxIntMass"] - ion + uncertancy,
    ];

    if (nrow(dfFound) == 0) {
      dfReturn[nrow(dfReturn)+1, ] <- as.data.frame(list(dfSearch[iRow,], FALSE, dfCol));
    } else {
      for (ii in 1:nrow(dfFound)) {
        dfReturn[nrow(dfReturn)+1, ] <- as.data.frame(list(dfSearch[iRow,], TRUE, dfFound[ii, ]));
      }
    }

    lc_progress_bar_step(pb, iRow);
  }
  lc_progress_bar_close(pb);
  lc_log("\nDone\n");

  return(dfReturn[-1, ]);
}

# Functions in this Section are for downloading databases -----

# this function downloads the bioinfo cristal database as json
# textfile
download_database.bioinfo_cristal <- function(
  url = "https://bioinfo.cristal.univ-lille.fr/norine/rest/peptides/json/smiles",
  destfile = paste(
    check_cache(),
    "/peptide_db.json", sep = ""
  ),
  update = FALSE
) {
  if (isTRUE(update) & file.exists(destfile)) file.remove(destfile);

  if (!file.exists(destfile)) {
    lc_log("Downloading bioinfo.cristal database\n");
    try(
        utils::download.file(
        url = url,
        destfile = destfile,
        method = "auto",
        quiet = FALSE
      )
    );
    if (! file.exists(destfile)) {
      file.copy(from = "inst/data/peptide_db.json", to = destfile);
    }
  }
  if (file.exists(destfile)) {
    return(destfile);
  } else {
    return(FALSE);
  }
}

# Rebuild the databasesystem. If you want to do this, hit this function. -----
# Necessary, if you have modified the entrys in the substances() function
update_databases <- function() {
  iStat <- 0;
  vFiles <- vector(mode = "character");
  # Paste the update functions for the databases in the Section above here
  vFiles <- c(vFiles, download_database.bioinfo_cristal(update = FALSE));

  # Final aggregation of databases and saving to file here
  vFiles <- c(vFiles, save_substances(update = TRUE));

  # Returns a string of created files and stat here
  if (any(isFALSE(vFiles))) iStat <- 1;
  return(list(
    stat = iStat,
    files = vFiles
  ));
}
