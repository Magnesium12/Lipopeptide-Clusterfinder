# Returns the substances data.frame.
# Function is vey time intensive, it is strongly recomended NOT running it
# directly.
substances <- function(
  bioinfo_cristal_json = download_database.bioinfo_cristal()
) {
  substance <- function(name = "Void", reduced = NA, mmes = NA,
    H = 0,                                                                                                                                                                                                                                                  He = 0,
    Li = 0, Be = 0,                                                                                                                                                                                                 B  = 0,  C = 0,  N = 0,  O = 0,  F = 0, Ne = 0,
    Na = 0, Mg = 0,                                                                                                                                                                                                 Al = 0, Si = 0, P  = 0,  S = 0, Cl = 0, Ar = 0,
    K  = 0, Ca = 0, Sc = 0,                                                                                                                 Ti = 0, V  = 0, Cr = 0, Mn = 0, Fe = 0, Co = 0, Ni = 0, Cu = 0, Zn = 0, Ga = 0, Ge = 0, As = 0, Se = 0, Br = 0, Kr = 0,
    Rb = 0, Sr = 0, Y  = 0,                                                                                                                 Zr = 0, Nb = 0, Mo = 0, Tc = 0, Ru = 0, Rh = 0, Pd = 0, Ag = 0, Cd = 0, In = 0, Sn = 0, Sb = 0, Te = 0, I  = 0, Xe = 0,
    Cs = 0, Ba = 0, La = 0, Ce = 0, Pr = 0, Nd = 0, Pm = 0, Sm = 0, Eu = 0, Gd = 0, Tb = 0, Dy = 0, Ho = 0, Er = 0, Tm = 0, Yb = 0, Lu = 0, Hf = 0, Ta = 0, W  = 0, Re = 0, Os = 0, Ir = 0, Pt = 0, Au = 0, Hg = 0, Tl = 0, Pb = 0, Bi = 0, Po = 0, At = 0, Rn = 0,
    Fr = 0, Ra = 0, Ac = 0, Th = 0, Pa = 0, U  = 0, Np = 0, Pu = 0, Am = 0, Cm = 0, Bk = 0, Cf = 0, Es = 0, Fm = 0, Md = 0, No = 0, Lr = 0, Rf = 0, Db = 0, Sg = 0, Bh = 0, Hs = 0, Mt = 0, Ds = 0, Rg = 0, Cn = 0, Nh = 0, Fl = 0, Mc = 0, Lv = 0, Ts = 0, Og = 0
  ){
    catchNull <- function(val) {
      if (! is.numeric(val)) {
        return(0);
      } else {
        return(val);
      }
    }

    # Mean mass
    rMeanMass <- 0;
    for (sEl in ELEMENTS$Symbol) {
      iAnzAtom <- eval(parse(text = sEl)); # Magic
      if (iAnzAtom == 0) next();
      if (nrow(ISOTOPES[ISOTOPES$Symbol == sEl,]) > 0) {
        rMeanMass <- rMeanMass + iAnzAtom * sum(
          ISOTOPES[ISOTOPES$Symbol == sEl, "Mass",] *
          ISOTOPES[ISOTOPES$Symbol == sEl, "Abundance",])/
        sum(ISOTOPES[ISOTOPES$Symbol == sEl, "Abundance",]
        );
      } else {
        rMeanMass <- NA;
        break();
      }
    }
    # Monoisotopic Mass
    rMonoisotopicMass <- 0;
    for (sEl in ELEMENTS$Symbol) {
      iAnzAtom <- eval(parse(text = sEl)); # Magic
      if (iAnzAtom == 0) next();
      if (nrow(ISOTOPES[ISOTOPES$Symbol == sEl,]) > 0) {
        dfIsotopes <- ISOTOPES[ISOTOPES$Symbol == sEl,];
        iMaxAbundance <- order(dfIsotopes$Abundance, decreasing = TRUE)[1];
        rMonoisotopicMass <- rMonoisotopicMass + iAnzAtom * dfIsotopes[iMaxAbundance, "Mass"];
      } else {
        rMonoisotopicMass <- FALSE;
        break();
      }
    }
    return(list(
      Name = name,
      "Mean mass" = rMeanMass,
      "Monoisotopic mass" = rMonoisotopicMass,
      "Measured mass" = mmes,
      "Reduced" = reduced,
      # Zusammensetzung
      H = H,                                                                                                                                                                                                                                                                                 He = He,
      Li = Li, Be = Be,                                                                                                                                                                                                                         B  = B,  C  = C,  N  = N,  O  = O,  F  = F,  Ne = Ne,
      Na = Na, Mg = Mg,                                                                                                                                                                                                                         Al = Al, Si = Si, P  = P,  S  = S,  Cl = Cl, Ar = Ar,
      K  = K,  Ca = Ca, Sc = Sc,                                                                                                                               Ti = Ti, V  = V,  Cr = Cr, Mn = Mn, Fe = Fe, Co = Co, Ni = Ni, Cu = Cu, Zn = Zn, Ga = Ga, Ge = Ge, As = As, Se = Se, Br = Br, Kr = Kr,
      Rb = Rb, Sr = Sr, Y  = Y,                                                                                                                                Zr = Zr, Nb = Nb, Mo = Mo, Tc = Tc, Ru = Ru, Rh = Rh, Pd = Pd, Ag = Ag, Cd = Cd, In = In, Sn = Sn, Sb = Sb, Te = Te, I  = I,  Xe = Xe,
      Cs = Cs, Ba = Ba, La = La, Ce = Ce, Pr = Pr, Nd = Nd, Pm = Pm, Sm = Sm, Eu = Eu, Gd = Gd, Tb = Tb, Dy = Dy, Ho = Ho, Er = Er, Tm = Tm, Yb = Yb, Lu = Lu, Hf = Hf, Ta = Ta, W  = W,  Re = Re, Os = Os, Ir = Ir, Pt = Pt, Au = Au, Hg = Hg, Tl = Tl, Pb = Pb, Bi = Bi, Po = Po, At = At, Rn = Rn,
      Fr = Fr, Ra = Ra, Ac = Ac, Th = Th, Pa = Pa, U  = U,  Np = Np, Pu = Pu, Am = Am, Cm = Cm, Bk = Bk, Cf = Cf, Es = Es, Fm = Fm, Md = Md, No = No, Lr = Lr, Rf = Rf, Db = Db, Sg = Sg, Bh = Bh, Hs = Hs, Mt = Mt, Ds = Ds, Rg = Rg, Cn = Cn, Nh = Nh, Fl = Fl, Mc = Mc, Lv = Lv, Ts = Ts, Og = Og
    ));
  }

  parse_database.bioinfo_cristal <- function(json) {
    v123 <- strsplit("0123456789", "")[[1]];
    vABC <- strsplit("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "")[[1]];
    vabc <- strsplit("abcdefghijklmnopqrstuvwxyz", "")[[1]];
    df_Peptide_DB <- jsonlite::fromJSON(txt = json);
    iNumberPeptides <- nrow(df_Peptide_DB$peptides);
    dfPeptides <- as.data.frame(substance());
    lc_log("Parsing JSON\n");
    pb <- lc_progress_bar_init(min = 0, max = iNumberPeptides, style = lc_config(key = "progress_bar.style") , label = "Smoothing");
    for (ii in 1:iNumberPeptides) {
      dfPeptide <- df_Peptide_DB$peptides[ii,]
      #print(dfPeptide)
      # Exceptions
      #print(dfPeptide$general$formula)
      if (dfPeptide$general$formula == "" | is.na(dfPeptide$general$formula)) next();

      H  <- 0;                                                                                                                                                                                                                                                                               He <- 0;
      Li <- 0; Be <- 0;                                                                                                                                                                                                                         B  <- 0;  C <- 0; N  <- 0;  O <- 0; F  <- 0; Ne <- 0;
      Na <- 0; Mg <- 0;                                                                                                                                                                                                                         Al <- 0; Si <- 0; P  <- 0;  S <- 0; Cl <- 0; Ar <- 0;
      K  <- 0; Ca <- 0; Sc <- 0;                                                                                                                               Ti <- 0; V  <- 0; Cr <- 0; Mn <- 0; Fe <- 0; Co <- 0; Ni <- 0; Cu <- 0; Zn <- 0; Ga <- 0; Ge <- 0; As <- 0; Se <- 0; Br <- 0; Kr <- 0;
      Rb <- 0; Sr <- 0; Y  <- 0;                                                                                                                               Zr <- 0; Nb <- 0; Mo <- 0; Tc <- 0; Ru <- 0; Rh <- 0; Pd <- 0; Ag <- 0; Cd <- 0; In <- 0; Sn <- 0; Sb <- 0; Te <- 0; I  <- 0; Xe <- 0;
      Cs <- 0; Ba <- 0; La <- 0; Ce <- 0; Pr <- 0; Nd <- 0; Pm <- 0; Sm <- 0; Eu <- 0; Gd <- 0; Tb <- 0; Dy <- 0; Ho <- 0; Er <- 0; Tm <- 0; Yb <- 0; Lu <- 0; Hf <- 0; Ta <- 0; W <- 0;  Re <- 0; Os <- 0; Ir <- 0; Pt <- 0; Au <- 0; Hg <- 0; Tl <- 0; Pb <- 0; Bi <- 0; Po <- 0; At <- 0; Rn <- 0;
      Fr <- 0; Ra <- 0; Ac <- 0; Th <- 0; Pa <- 0; U  <- 0; Np <- 0; Pu <- 0; Am <- 0; Cm <- 0; Bk <- 0; Cf <- 0; Es <- 0; Fm <- 0; Md <- 0; No <- 0; Lr <- 0; Rf <- 0; Db <- 0; Sg <- 0; Bh <- 0; Hs <- 0; Mt <- 0; Ds <- 0; Rg <- 0; Cn <- 0; Nh <- 0; Fl <- 0; Mc <- 0; Lv <- 0; Ts <- 0; Og <- 0;

      vsName <- strsplit(dfPeptide$general$formula, "")[[1]];
      vsName <- c(vsName, "X", "x");
      sSym <- "";
      sNumAtom <- "";
      repeat { # Parse sumformula
        #Sys.sleep(1)
        #print(sSym);
        #print(sNumAtom);
        #print(vsName[1]);
        if (any(vsName[1] == vABC)) {
          if (! sSym == "") {
            # Set element
            if (sNumAtom == "") {
              iNumAtom <- 1;
            } else {
              iNumAtom <- as.numeric(sNumAtom);
            }
            if (any(sSym ==  ELEMENTS$Symbol)) {
              eval(parse(text = paste(
                sSym, " <- ", iNumAtom, ";", # Set e.g. H to number
              sep = "")));
            } else {
              cat("Invalid element symbol!")
              return(FALSE);
              #break(2);
            }
          }
          sSym <- vsName[1];
          sNumAtom <- "";
          vsName <- vsName[-1];
        } else if (any(vsName[1] == vabc)) {
          sSym <- paste(sSym, vsName[1], sep = "");
          vsName <- vsName[-1];
        } else if (any(vsName[1] == v123)) {
          sNumAtom <- paste(sNumAtom, vsName[1], sep = "");
          vsName <- vsName[-1];
        } else if (any(vsName[1] == " ")) {
          # If somebody put shit like " " in the formula
          vsName <- vsName[-1];
        } else {
          cat("What the fuck have you done?!?!\n");
          return(FALSE);
        }
        if (sSym == "Xx") break();
      }

      dfPeptides <- rbind(dfPeptides, as.data.frame(substance(
        name = dfPeptide$general$name, reduced = FALSE, mmes = dfPeptide$general$mw,
        H = H,                                                                                                                                                                                                                                                                                 He = He,
        Li = Li, Be = Be,                                                                                                                                                                                                                         B = B,   C = C,   N = N,   O = O,   F = F,   Ne = Ne,
        Na = Na, Mg = Mg,                                                                                                                                                                                                                         Al = Al, Si = Si, P = P,   S = S,   Cl = Cl, Ar = Ar,
        K = K,   Ca = Ca, Sc = Sc,                                                                                                                               Ti = Ti, V = V,   Cr = Cr, Mn = Mn, Fe = Fe, Co = Co, Ni = Ni, Cu = Cu, Zn = Zn, Ga = Ga, Ge = Ge, As = As, Se = Se, Br = Br, Kr = Kr,
        Rb = Rb, Sr = Sr, Y = Y,                                                                                                                                 Zr = Zr, Nb = Nb, Mo = Mo, Tc = Tc, Ru = Ru, Rh = Rh, Pd = Pd, Ag = Ag, Cd = Cd, In = In, Sn = Sn, Sb = Sb, Te = Te, I = I,   Xe = Xe,
        Cs = Cs, Ba = Ba, La = La, Ce = Ce, Pr = Pr, Nd = Nd, Pm = Pm, Sm = Sm, Eu = Eu, Gd = Gd, Tb = Tb, Dy = Dy, Ho = Ho, Er = Er, Tm = Tm, Yb = Yb, Lu = Lu, Hf = Hf, Ta = Ta, W = W,   Re = Re, Os = Os, Ir = Ir, Pt = Pt, Au = Au, Hg = Hg, Tl = Tl, Pb = Pb, Bi = Bi, Po = Po, At = At, Rn = Rn,
        Fr = Fr, Ra = Ra, Ac = Ac, Th = Th, Pa = Pa, U = U,   Np = Np, Pu = Pu, Am = Am, Cm = Cm, Bk = Bk, Cf = Cf, Es = Es, Fm = Fm, Md = Md, No = No, Lr = Lr, Rf = Rf, Db = Db, Sg = Sg, Bh = Bh, Hs = Hs, Mt = Mt, Ds = Ds, Rg = Rg, Cn = Cn, Nh = Nh, Fl = Fl, Mc = Mc, Lv = Lv, Ts = Ts, Og = Og
      )));
      lc_progress_bar_step(pb, ii)
    }
    lc_progress_bar_close(pb);
    lc_log("\nDone\n");
    dfPeptides <- dfPeptides[-1,];
    return(dfPeptides);
  }

  # EDIT HERE
  private_data <- list();
  public_data <- list();

  private_data$amino_acids <- rbind.data.frame(
    substance(name = "Glycin", C = 2, H = 5, N = 1, O = 2),
    substance(name = "Glycin", reduced = TRUE, C = 2, H = 3, N = 1, O = 1)
  );
  private_data$lipopeptides <- rbind.data.frame(
    substance(name = "Surfactin", C = 53, H = 93, N = 7, O = 13)
  );
  # Public Databases ### EDIT
  #tryCatch({
  public_data$bioinfo_cristal <- parse_database.bioinfo_cristal(
    json = bioinfo_cristal_json
  );
  if (isFALSE(public_data$bioinfo_cristal)) {
    # If parsing failed, a old copy from package is used
    cat("Parsing json failed!");
    file.copy(from = "inst/data/peptide_db.json", to = bioinfo_cristal_json);
    public_data$bioinfo_cristal <- parse_database.bioinfo_cristal(
      json = bioinfo_cristal_json
    );
  }


  dfSubstances <- rbind(
    cbind(private_data$amino_acids, list(Database = "Private_amino_acids")),
    cbind(private_data$lipopeptides, list(Database = "Private_lipopeptides")),
    cbind(public_data$bioinfo_cristal, list(Database = "Public_Bioinfo_cristal"))
  );

  dfSubstances <- dfSubstances[ # Remove zero columns
    , apply(dfSubstances, 2, function(column) {
      if (all(column == 0)) {
        return(FALSE);
      } else {
        return(TRUE);
      }
    })
  ];
  return(dfSubstances);
}

# Build the substances database and saves it in a file.
# The function substances() would be started
save_substances <- function(
  dfSubstances = substances(),
  destfile = paste(check_cache(), "/substances.rds", sep = ""),
  update = FALSE
) {
  if (isTRUE(update) & file.exists(destfile)) file.remove(destfile);

  if (!file.exists(destfile)) saveRDS(object = dfSubstances, file = destfile);
  return(destfile);
}

# Load the substances data.frame from rds file and returns it.
# If the file doesn't exist, it would be builded
get_substances <- function(file = paste(check_cache(), "/substances.rds", sep = "")) {
  if (!file.exists(file)) save_substances();
  dfSubstances <- readRDS(file = file);
  return(dfSubstances);
}
