# Log functions
lc_log <- function(...) {
 if (isTRUE(lc_config(key = "verbose"))) base::cat(...);
}

lc_progress_bar_init <- function(...) {
 if (isTRUE(lc_config(key = "verbose"))) {
   return(utils::txtProgressBar(...));
 } else {
   return(FALSE);
 }
}

lc_progress_bar_step <- function(...) {
 if (isTRUE(lc_config(key = "verbose"))) {
   return(utils::setTxtProgressBar(...));
 } else {
   return(FALSE);
 }
}

lc_progress_bar_close <- function(...) {
 if (isTRUE(lc_config(key = "verbose"))) {
   return(base::close(...));
 } else {
   return(FALSE);
 }
}
