globalVariables(c(
  #"LIPOPEPTIDE_CLUSTERFINDER_CONFIG",
  "TIME",
  "ELEMENTS",
  "ISOTOPES"
));
lpcfind.globals <- new.env();

# Configuration
lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG <- list(
  verbose = TRUE,
  progress_bar = list(
    style = 3
  ),
  cache = list(
    dir = "none"
  )
);

#' Change global options
#' @param key Select the configuration variable you want to set or get
#' \code{"verbose"}, \code{"progress_bar.style"} or \code{"cache.dir"} are
#' currently possible
#' @param value Set the value of the selected variable. If not set,
#' or set to NA, the actual value gets returned
#' @return Nothing or value of variable
lc_config <- function(key = "", value = NA) {
  switch (key,
    verbose = {
      if (is.na(value)) {
        return(lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$verbose);
      } else {
        if (isTRUE(value)) {
          lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$verbose <- TRUE;
        } else {
          lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$verbose <- FALSE;
        }
      }
    },
    "progress_bar.style" = {
      if (is.na(value)) {
        return(lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$progress_bar$style);
      } else {
        if (value >= 1 & value <= 3) {
          lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$progress_bar$style <- value;
        } else {
          cat("Warning: invalid value, nothing changed.")
          #lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$progress_bar$style <- 3;
        }
      }
    },
    "cache.dir" = {
      if (is.na(value)) {
        return(lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$cache$dir);
      } else {
        if (dir.exists(value)) {
          lpcfind.globals$LIPOPEPTDE_CLUSTERFINDER_CONFIG$cache$dir <- value;
        } else {
          stop("Invalid dir");
        }
      }
    },
    {
      cat("Info: No valid key selected.\n");
    }
  );
}

# get cache dir and check existence
check_cache <- function() {
  if (
    ! dir.exists(lc_config(key = "cache.dir"))
  ){ ###
    cache_dir <- R.cache::getCacheRootPath(defaultPath="~/.lpcfind-cache");
    Sys.sleep(0.1);
    lc_config(key = "cache.dir", value = cache_dir);
  }
  return(lc_config(key = "cache.dir"));
}

time_list <- function() {
  second <- 1;
  hour <- 3600 * second;
  day <- 24 * hour;
  week <- 7 * day;
  month <- 30 * day;
  year <- 365.25 * day;
  return(list(
    second = second,
    hour = hour,
    day = day,
    week = week,
    month = month,
    year = year
  ));
}
TIME <- time_list();
rm("time_list");
