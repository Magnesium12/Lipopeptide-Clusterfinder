
#' prepare_data: get called by find Cluster
#'
#' In this function all Data Preprocessing is handled
#'
#' The Data is first smoothed with the defined smoothing function
#' then the baseline corrected with the defined baseline correction method.
#'
#' @param dfSpectrum Raw spectrum, a data.frame with a mass and an intensity column
#' @param smoothing_method Defines the selected Smoothing method
#' @param window_half_width Half width of smoothing window
#' @param subtract_baseline_method Defines the selected baseline correction
#' @param subtract_baseline_correction_factor Baseline threshold gets multiplied with this factor
#'
#' @export

prepare_data <- function(dfSpectrum,
  smoothing_method = "moving_average", window_half_width = 2,
  subtract_baseline_method = "none", subtract_baseline_correction_factor = 1
){
  # Smoothing functions: -----

  # Moving Average
  #
  # Moving average smoothing with a defined window size of param hwin for each datapoint.
  # For each Datapoint the average of  A - @param hwin to A + param hwin is caclulated
  # and is safed as the smoothed Datapoint.

  # @param data Dataframe with numerical values
  # @param hwin Defines distance in each direction for averaging
  # @param oor out of reach for smoothing funtion

  moving_average <- function(data, hwin = 2, oor = NA) {
    vData <- as.vector(data);
    vSmoothed <- vector(mode = "numeric", length = length(vData));
    vSmoothed[] <- oor;
    lc_log("Smoothing with moving average\n");
    pb <- lc_progress_bar_init(min = (hwin + 1), max = (length(vData) - hwin), style = lc_config(key = "progress_bar.style") , label = "Smoothing"); # progression bar
    for (ii in (hwin + 1):(length(vData) - hwin)) {
      vSmoothed[ii] <- mean(vData[(ii-hwin):(ii+hwin)]); # step by step smoothing
      lc_progress_bar_step(pb, ii)
    }
    lc_progress_bar_close(pb);
    lc_log("\nDone\n");
    return(vSmoothed);
  }

  # Baseline correction functions: -----

  # Data smoothing -----
  dfPrepared <- dfSpectrum;
  switch (smoothing_method,
    "none" = {},
    "moving_average" = {
      dfPrepared$intensity <- moving_average(
        data = dfPrepared$intensity, hwin = window_half_width, oor = NA
      );
    },
    {stop("Fatal Error!");}
  );

  # Baseline correction, after smoothing! ------
  rBaselineCorrectionValue <- 0;
  if (subtract_baseline_method == "mean" | subtract_baseline_method == "median") {
    # Subtract mean * correction-factor
    switch (subtract_baseline_method,
      "mean"   = {rBaselineCorrectionValue <- (subtract_baseline_correction_factor * base::mean(   dfPrepared$intensity, na.rm = TRUE));},
      "median" = {rBaselineCorrectionValue <- (subtract_baseline_correction_factor * stats::median(dfPrepared$intensity, na.rm = TRUE));}
    );
    dfPrepared$intensity <- dfPrepared$intensity - rBaselineCorrectionValue;
    # Set Values lower than 0 to 0
    vIntensity <- dfPrepared$intensity;
    vIntensity[is.na(vIntensity)] <- 0; # Migitate Problems with NA
    dfPrepared$intensity[vIntensity < 0] <- 0;
  }

  return(list(
    stat = 0,
    spectrum = dfPrepared,
    subtract_baseline_correction_value = rBaselineCorrectionValue
  ));
}

#' find_peaks : get called by find Cluster
#'
#' It finds peaks
#'
#' After smoothing and basline Corrections are done the spectra is seperated into peaks
#' based on a continuity criteria, all Datapoint from a local maxima to the next local minima
#' are classified as datapoints from the same peak. For these peaks a dataframe is crated with
#' the left and rigth borders of the peak, the total Intensity and the m/z Value of the peak maximum.
#'
#' After checking for Calculation minima this Dataframe dfPeaks is then returned.
#' @param dfSmoothed Smoothed spectrum, a data.frame with a mass and an intensity column.
#' @param minIntCalc minimum single datapoint Value witch will be considered for calculation
#' @param minIntPeak minimum peak Value witch will be considered for calculation
#'
#' @export
find_peaks <- function(dfSmoothed, minIntCalc = 400, minIntPeak = 4000) {
  dfPeaks <- data.frame(
    leftBorder  = NA,
    rightBorder = NA,
    maxIntMass  = NA,
    intensity   = NA
  );

  # dfTest[order(dfTest$intensity, decreasing = TRUE),][1,]
  iPeaks <- 1;
  lc_log("Estimating Peak:\n")
  repeat { # Horrifying break() system, no better solution possible
    lc_log(iPeaks, " ");
    switch(round(((iPeaks %% 12*8) + 1)/12),
           lc_log('|       |'),     #         #    #######
           lc_log('|#      |'),     #      #       #
           lc_log('| #     |'),     #     #        #
           lc_log('|  #    |'),     #    #         #––––––
           lc_log('|   #   |'),     #    #         #––––––
           lc_log('|    #  |'),     #     #        #
           lc_log('|     # |'),     #      #       #
           lc_log('|      #|')      #         #    #######
    );
    lc_log("\r");
    # Max of Spektrum
    iMax <- order(dfSmoothed$intensity, decreasing = TRUE, na.last = TRUE)[1];
    iMaxInt <- dfSmoothed[iMax, "intensity"];
    if (is.na(iMaxInt) || (iMaxInt < minIntCalc)) break();

    vlr <- c(-1, 1); # Left or right
    vBorderIndex <- c(0, 0);
    vBreakDim <- c(0, nrow(dfSmoothed));
    for (ii in 1:2) {
      ic <- 0; # Left/Right border, its complicated
      while (
        vlr[ii]*(iMax + vlr[ii]*(ic + 1)) < vlr[ii]*vBreakDim[ii] &&             # limits of data.frame check
        (!is.na((dfSmoothed[(iMax + vlr[ii]*(ic + 1)), "intensity"]))) &&        # is na? peak previously was removed
        (dfSmoothed[(iMax + vlr[ii]*(ic + 1)), "intensity"] > minIntCalc) &&     # threshold for intensity
        (dfSmoothed[(iMax + vlr[ii]*(ic + 1)), "intensity"] > 0) &&              # if zero, no peak - necessary if minintcalc not set
        (dfSmoothed[(iMax + vlr[ii]*(ic + 1)), "intensity"]) < (dfSmoothed[(iMax + vlr[ii]*ic), "intensity"]) # is next point larger or equal than previous?
      ){
        ic <- ic + 1
      }
      vBorderIndex[ii] <- iMax + vlr[ii]*ic;
    }
    # If Peaks don't have a width, break()
    if (vBorderIndex[1] == vBorderIndex[2]) break();
    vNewRow <- c( # New peak
      dfSmoothed[vBorderIndex[1], "mass"],
      dfSmoothed[vBorderIndex[2], "mass"],
      dfSmoothed[iMax, "mass"],
      sum(
        dfSmoothed[vBorderIndex[1]:vBorderIndex[2], "intensity"]
      )
    );
    dfPeaks <- rbind(dfPeaks, vNewRow); # add new peak
    dfSmoothed[vBorderIndex[1]:vBorderIndex[2],][] <- NA; # Remove recognized Peak
    #dfSmoothed <- dfSmoothed[-1*vBorderIndex[1]:vBorderIndex[2],];
    iPeaks <- iPeaks + 1;
    if (all(is.na(dfSmoothed))) break(); # if all is na
  }
  lc_log("\nDone\n");

  # Nice output like a cat
  dfPeaks <- dfPeaks[-1, ];
  dfPeaks <- dfPeaks[order(dfPeaks$intensity, decreasing = TRUE),]
  dfPeaks <- dfPeaks[dfPeaks$intensity > minIntPeak,];
  return(list(
    stat = 0,
    peak_table = dfPeaks,
    Rest = dfSmoothed
  ));
}


